const ig = require('instagram-node').instagram();
ig.use({access_token: process.env.INSTAGRAM_TOKEN});

exports.handler = (req, res) => {
  const count = req.query.count || 9;
  res.set('Access-Control-Allow-Origin', '*');
  
  if (req.method === 'OPTIONS') {
    res.set('Access-Control-Allow-Methods', 'GET');
    res.set('Access-Control-Allow-Headers', 'Content-Type');
    res.set('Access-Control-Max-Age', '3600');
    res.status(204).send('');
    return;
  }
  
  ig.user_self_media_recent({count}, (err, medias, pagination, remaining, limit) => {
    if (err) {
      return res.send(err.body)
    }
    res.send(medias)
  })
};
